# Título del Proyecto

_Prueba "COGNOX"_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Composer versioversiónn actualizada
MySQL
PHP >= 7.1.3
```

### Instalación 🔧

_El proyecto esta hecho en laravel 5.8_

_Necesitaras de la ayuda de composer para descargar los paquetes necesarios para el funcionamiento_

_Asegurate de tener creada la base de datos con nombre "cognox"._

```
    composer install
```

_Crea un archivo .env en la raiz del proyecto._

_Copea y pega las siguiente lineas de código en el archivo .env creado, (las credenciales de la base de datos pueden cambiar de acuerdo a las configuraciones que tengas localmente, puedes revisar las variables con el prefijo "DB" y ver si coinciden con las que tienes localmente, de lo contrario siéntete libre de modificarlas a conveniencia) ._

```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:ead2tUM26DQRUWFgH0opJHdTl6TTE7jQ6VINWkZ0TSs=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=cognox
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

```

_A continuación corre el siguiente (este comando creara las tablas y generara los datos iniciales)._

```
 php artisan migrate:refresh --seed 
```

## Autor ✒️

* **Jhonathan Ascencio** - *Prueba COGNOX* 



## Agradecimientos a COGNOX 🎁
