@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card">
            <table class="table">
                <thead>
                <tr>
                    <th>NUMERO DE CUENTA</th>
                    <th>SALDO</th>
                    <th>ACTIVO</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($cuentas as $cuenta)
                        <tr>
                            <td>{{$cuenta->numero_cuenta}}</td>
                            <td>{{$cuenta->saldo}}</td>
                            <td>@if ($cuenta->activo === 1) 
                                    ACTIVA
                                @else 
                                    INACTIVA
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if (\Session::has('msg'))
                    <div class="alert alert-primary">
                        {!! \Session::get('msg') !!}
                    </div>
                @endif
            <div class="card-header">{{ __('Transaccion') }}</div>
                @if(count($cuentas) >= 2)
                    <div class="card-body">
                        <form method="POST" action="{{ url('transferir') }}">
                            <div class="form-group">
                                <label for="email">Cuenta origen:</label>
                                <select class="form-control @error('cuenta_origen') is-invalid @enderror" name="cuenta_origen" id="cuenta_origen">
                                    <option>Seleccione cuenta destino</option>
                                    @foreach($cuentas as $cuenta)
                                        <option value="{{$cuenta->id}}">{{$cuenta->numero_cuenta}}</option>
                                    @endforeach
                                </select>
                                @error('cuenta_origen')
                                    <span class="invalid-feedback " role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="pwd">Cuenta destino :</label>
                                <select class="form-control @error('cuenta_destino') is-invalid @enderror" name="cuenta_destino" id="cuenta_destino">
                                    <option>Seleccione cuenta de origen</option>
                                    @foreach($cuentas as $cuenta)
                                        <option value="{{$cuenta->id}}">{{$cuenta->numero_cuenta}}</option>
                                    @endforeach
                                </select>
                                @error('cuenta_destino')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="valor">Valor de la transacción:</label>
                                <input type="number" class="form-control @error('valor') is-invalid @enderror" name="valor" id="valor">
                                @error('valor')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Transferir</button>
                        </form>
                    </div>
                @else
                    <div class="card-body">
                        No dispone de cuentas suficientes para hacer transferencias entre cuentas propias.
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
