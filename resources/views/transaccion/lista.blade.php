@extends('layouts.app')

@section('content')
<div class="container ">
    <form method="get" action="">
        <div class="input-group mb-3">
            <button type="button" class="btn btn-outline-secondary" onclick="filtrar()">Filtrar</button>
            <label for="staticEmail" class="col-sm-2 col-form-label">Cuenta de origen</label>
            <input type="number" id="filtroCuentaO" class="form-control" aria-label="Text input with segmented dropdown button" value="0">
            <label for="staticEmail" class="col-sm-2 col-form-label">Cuenta destino</label>
            <input type="number" id="filtroCuentaD" class="form-control" aria-label="Text input with segmented dropdown button" value="0">
        </div>
    </form>
    <table class="table  table-sm">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Cuenta origen</th>
                <th scope="col">Cuenta destino</th>
                <th scope="col">Valor</th>
                <th scope="col">Fecha transaccion</th>
                <th scope="col">Tipo trasaccion</th>
            </tr>
        </thead>
        @if(count($transacciones) >= 2)
            <tbody>
                @foreach ($transacciones as $transaccion)
                    @if(Auth::user()->id == $transaccion->userD_id && Auth::user()->id == $transaccion->userO_id)
                        @php($tipoTrans = 'Cuenta propia') 
                    @elseif(Auth::user()->id == $transaccion->userD_id)
                        @php($tipoTrans = 'A favor')  
                    @else 
                        @php($tipoTrans = 'En contra')  
                    @endif                                        
                    <tr class="@if($tipoTrans == 'Cuenta propia') text-dark @elseif($tipoTrans == 'A favor') text-success @else text-danger @endif">
                        <td scope="col">{{ $transaccion->id }}</td>
                        <td scope="col">{{ $transaccion->cuenta_origen }}</td>
                        <td scope="col">{{ $transaccion->cuenta_destino }}</td>
                        <td scope="col">{{ $transaccion->valor }}</td>
                        <td scope="col">{{ $transaccion->created_at }}</td>
                        <td scope="col">
                            {{$tipoTrans}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        @else
            <div class="card-body">
                No hay un listado de datos para mostrar.
            </div>
        @endif
    </table>
    {{ $transacciones->links() }}
</div>
<script type="text/javascript">
    function filtrar(){
        let filtroCuentaO = $('#filtroCuentaO').val();
        let filtroCuentaD = $('#filtroCuentaD').val();
        $(location).attr('href',window.location.hash+'/transacciones/'+filtroCuentaO+'/'+filtroCuentaD);
        
    }
</script>
@endsection
