<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('auth');;

Auth::routes();
Route::post('/login', 'Auth\AuthController@login');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/cuentaP', 'TransaccionesController@cuentasP')->middleware('auth');
Route::get('/cuentaT', 'TransaccionesController@cuentasT')->middleware('auth');
Route::post('/transferir', 'TransaccionesController@transferir')->middleware('auth');
Route::get('/transacciones/{CuentaOrigen?}/{cuentaDestino?}', 'TransaccionesController@ListarTransacciones')->middleware('auth');