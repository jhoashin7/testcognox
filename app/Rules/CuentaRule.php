<?php

namespace App\Rules;

use App\Cuenta;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class CuentaRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $cuenta =  Cuenta::find($value);
        if ($cuenta->user_id === Auth::user()->id){
            return true ;
        }else{
            return false
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */ 
    public function message()
    {
        return 'this count no below user.';
    }
}
