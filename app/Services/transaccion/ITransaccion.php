<?php

namespace App\Services\Transaccion;

interface ITransaccion {
    public function CuentasPropias();
    public function CuentasVinculadas();
    public function transferir($data);
    public function listarTransacciones($CuentaOrigen ,$cuentaDestino);
   

}