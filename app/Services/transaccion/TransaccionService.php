<?php 
namespace App\Services\Transaccion;

use App\Cuenta;
use App\CuentaVinculada;
use App\Services\Transaccion\ITransaccion;
use App\Transaccion;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TransaccionService implements ITransaccion{
    public function CuentasPropias()
    {
        $cuentas =  Cuenta::where('user_id',Auth::user()->id)->get();
        return $cuentas;
    }
    
    public function CuentasVinculadas()
    {
        $cuentaVinculada = CuentaVinculada::cuentas();
        return $cuentaVinculada;
    }

    public function transferir($data){
        try{
            $validarSaldo = $this->validarSaldo($data['cuenta_origen'],$data['valor']);
            if(!$validarSaldo){
                return "El valor a transferir supera el saldo de la cuenta origen";
            }
            DB::beginTransaction();
                $transaccion = Transaccion::create($data);
                Cuenta::find($data['cuenta_origen'])->decrement('saldo',$data['valor']);
                Cuenta::find($data['cuenta_destino'])->increment('saldo',$data['valor']);
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            $transaccion = $e;
        }
        return "La transacción fue creada con el codigo ".$transaccion->id;
    }

    public function listarTransacciones($CuentaOrigen ,$cuentaDestino){
        return Transaccion::listarTransacciones($CuentaOrigen ,$cuentaDestino);
    }

    public function validarSaldo($cuenta, $valorTransaccion){
        $cuenta = Cuenta::find($cuenta);
        return ($cuenta->saldo >= $valorTransaccion) ? true : false;
    }

}