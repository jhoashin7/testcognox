<?php

namespace App\Providers;

use App\Services\Transaccion\TransaccionService;
use App\Services\Transaccion\ITransaccion;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ITransaccion::class,
                         TransaccionService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
