<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Transaccion extends Model
{
    protected $table = 'transacciones';

    protected $fillable = [
        'id','cuenta_origen','cuenta_destino','valor'
    ];

    public static function listarTransacciones($CuentaOrigen, $cuentaDestino){
        $filtro=  "userO.id is not null ";
        $filtro.= ($CuentaOrigen != 0)? "and cuentaO.numero_cuenta = $CuentaOrigen ":'';
        $filtro.= ($cuentaDestino != 0)? "and cuentaD.numero_cuenta = $cuentaDestino":'';
        $transacciones = Transaccion::join('cuentas as cuentaO', 'cuentaO.id','=','transacciones.cuenta_origen')
                                    ->join('cuentas as cuentaD', 'cuentaD.id','=','transacciones.cuenta_destino')
                                    ->join('users as userO', 'cuentaO.user_id','=','userO.id')
                                    ->join('users as userD', 'cuentaD.user_id','=','userD.id')
                                    ->whereRaw($filtro)
                                    ->where('userO.id',Auth::user()->id)
                                    ->orWhere('userD.id',Auth::user()->id)
                                    ->whereRaw($filtro)
                                    ->selectRaw('transacciones.id,transacciones.valor,transacciones.created_at,cuentaO.numero_cuenta cuenta_origen, cuentaD.numero_cuenta cuenta_destino, userO.id userO_id, userO.name userO_name,userD.id userD_id,userD.name userD_name')
                                    ->orderBy("transacciones.id")
                                    ->paginate(10);
        return $transacciones;
    }
}
