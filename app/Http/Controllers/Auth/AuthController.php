<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\User;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LoginRequest $request){
        if (Auth::attempt(array('code'=>$request->code,'password'=>$request->password))) 
        {
            $user = User::where('code',$request->code)->first();
            Session::put('user', $user); 
        }
        return Redirect('/');
        
    }
}
