<?php

namespace App\Http\Controllers;

use App\Cuenta;
use App\CuentaVinculada;
use App\Http\Requests\TransaccionRequest;
use App\Services\Transaccion\ITransaccion;
use App\Transaccion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class TransaccionesController extends Controller
{
    private $_iTransaccion;

    public function __construct(ITransaccion $iTransaccion){

        $this->_iTransaccion = $iTransaccion;
    }


    public function cuentasP(){

        return view('transaccion.cuentaP', ['cuentas' =>  $this->_iTransaccion->CuentasPropias()]);
    }


    public function cuentasT(){

        return view('transaccion.cuentaT', 
                    [
                        'cuentas' => $this->_iTransaccion->CuentasPropias(),
                        'cuentas_vinculadas' => $this->_iTransaccion->CuentasVinculadas()
                    ]);
    }


    public function transferir(TransaccionRequest $request ){

        return Redirect::back()->with('msg', $this->_iTransaccion->transferir($request->all()));
    }

    
    public function ListarTransacciones($CuentaOrigen = 0  ,$cuentaDestino = 0 ){

        return view('transaccion.lista', ['transacciones' => $this->_iTransaccion->listarTransacciones($CuentaOrigen ,$cuentaDestino)]);
    }
}
