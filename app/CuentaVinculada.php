<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CuentaVinculada extends Model
{
    protected $table = 'cuentas_vinculadas';

    protected $fillable = [
        'id','cuenta_id','user_id'
    ];

    public static function cuentas()
    {
        return CuentaVinculada::join('cuentas','cuentas.id','=','cuentas_vinculadas.cuenta_id')
                              ->where('cuentas_vinculadas.user_id',Auth::user()->id)->get();
    }
}
