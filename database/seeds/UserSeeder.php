<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'prueba1',
            'code'=>'1234',
            'password'=>bcrypt('1234'),                        
            'created_at'=>DB::raw('now()'),            
        ]);
        DB::table('users')->insert([
            'name'=>'prueba2',
            'code'=>'5467',
            'password'=>bcrypt('5467'),                        
            'created_at'=>DB::raw('now()'),            
        ]);
        DB::table('users')->insert([
            'name'=>'prueba3',
            'code'=>'7897',
            'password'=>bcrypt('7897'),                        
            'created_at'=>DB::raw('now()'),            
        ]);
    }
}
