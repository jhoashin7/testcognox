<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CuentaVinculadaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cuentas_vinculadas')->insert([
            'cuenta_id'=>'3',
            'user_id'=>'1',                     
            'created_at'=>DB::raw('now()'),            
        ]);
        DB::table('cuentas_vinculadas')->insert([
            'cuenta_id'=>'4',
            'user_id'=>'1',                     
            'created_at'=>DB::raw('now()'),            
        ]);
        DB::table('cuentas_vinculadas')->insert([
            'cuenta_id'=>'5',
            'user_id'=>'1',                     
            'created_at'=>DB::raw('now()'),            
        ]);
        DB::table('cuentas_vinculadas')->insert([
            'cuenta_id'=>'1',
            'user_id'=>'2',                     
            'created_at'=>DB::raw('now()'),            
        ]);
        DB::table('cuentas_vinculadas')->insert([
            'cuenta_id'=>'5',
            'user_id'=>'2',                     
            'created_at'=>DB::raw('now()'),            
        ]);
        DB::table('cuentas_vinculadas')->insert([
            'cuenta_id'=>'3',
            'user_id'=>'1',                     
            'created_at'=>DB::raw('now()'),            
        ]);
        
    }
}
