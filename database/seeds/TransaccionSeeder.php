<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransaccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0 ; $i < 20 ; $i ++ ){
            DB::table('transacciones')->insert([
                'cuenta_origen'=>'1',
                'cuenta_destino'=>rand(2, 5),  
                'valor' =>100000,
                'created_at'=>DB::raw('now()'),            
            ]);
        }
        for($i = 0 ; $i < 20 ; $i ++ ){
            DB::table('transacciones')->insert([
                'cuenta_origen'=>rand(2, 5),
                'cuenta_destino'=>1,  
                'valor' =>100000,
                'created_at'=>DB::raw('now()'),            
            ]);
        }
    }
}
