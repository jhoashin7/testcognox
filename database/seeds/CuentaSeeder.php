<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CuentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cuentas')->insert([
            'user_id'=>'1',
            'numero_cuenta'=>'123456789',
            'activo'=>1, 
            'saldo'=>1000000,                        
            'created_at'=>DB::raw('now()'),            
        ]);
        DB::table('cuentas')->insert([
            'user_id'=>'1',
            'numero_cuenta'=>'321654987',
            'activo'=>1,                        
            'saldo'=>100,                        
            'created_at'=>DB::raw('now()'),            
        ]);
        DB::table('cuentas')->insert([
            'user_id'=>'2',
            'numero_cuenta'=>'123789456',
            'activo'=>1,                        
            'saldo'=>10000,
            'created_at'=>DB::raw('now()'), 
        ]);
        DB::table('cuentas')->insert([
            'user_id'=>'2',
            'numero_cuenta'=>'789123456',
            'activo'=>1,  
            'saldo'=>1000000,                      
            'created_at'=>DB::raw('now()'), 
        ]);
        DB::table('cuentas')->insert([
            'user_id'=>'3',
            'numero_cuenta'=>'159736248',
            'activo'=>1,
            'saldo'=>100000,                    
            'created_at'=>DB::raw('now()'), 
        ]);
    }
}
